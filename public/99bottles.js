/**
 * Created by Fara Aileen on 31/3/2016.
 */
for(var beerOnWall=99; beerOnWall > 0; beerOnWall--){
    for(var bottleCount=beerOnWall; bottleCount > 0; bottleCount--) {
        document.write("<img src='/images/beer_bottle.png'>")
    }
    switch(beerOnWall){
        case 2:
            document.write("<p>2 bottles of beer on the wall, 2 bottles of beer." +
                "<br>If one of those beers should happen to fall, 1 bottle of beer on the wall...</p><br>");
            break;
        case 1:
            document.write("<p>1 bottle of beer on the wall, 1 bottle of beer." +
                "<br>If that beer should happen to fall, 0 bottles of beer on the wall...</p><br>");
            break;
        default:
            document.write("<p>" + beerOnWall + " bottles of beer on the wall, " + beerOnWall + " bottles of beer." +
                "<br>If one of those beers should happen to fall, " + (beerOnWall - 1) + " bottles of beer on the wall...</p><br>");
    }
}

document.write("<p>No more bottles of beer on the wall, no more bottles of beer." +
    "<br>Go to the store and buy some more, 99 bottles of beer on the wall...</p>");