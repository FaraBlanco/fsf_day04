/**
 * Created by Fara Aileen on 31/3/2016.
 */

// Load Express
var express = require("express");

// Create an application
var app = express();

// Define document root
// Specify where static files will be served from
app.use(express.static(__dirname + "/public"));

// Start a server
// Specify a port to listen to
// Execute function() after server started listening on port 3000
app.listen(3000, function() {
    console.info("You've reached port 3000. We're glad you made it!")
});
